#
#   Copyright © 2021,2022 IsardVDI S.L.
#   Copyright © 2022 Evilham <contact@evilham.com>
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
import logging as log
import os
import traceback

from pprint import pprint

from minio import Minio
from minio.commonconfig import REPLACE, CopySource
from minio.deleteobjects import DeleteObject
from requests import get, post

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from admin.flaskapp import AdminFlaskApp


# TODO: Fix all this
def get_legal(app : "AdminFlaskApp", lang : str) -> str:
    with open(app.legal_path+lang, "r") as languagefile:
        return languagefile.read()

def gen_legal_if_not_exists(app : "AdminFlaskApp", lang : str) -> None:
    if not os.path.isfile(app.legal_path+lang):
        log.debug("Creating new language file")
        with open(app.legal_path+lang, "w") as languagefile:
            languagefile.write("<b>Legal</b><br>This is the default legal page for language " + lang)

def new_legal(app : "AdminFlaskApp", lang : str, html : str) -> None:
    with open(app.legal_path+lang, "w") as languagefile:
        languagefile.write(html)
