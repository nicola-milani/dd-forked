#
#   Copyright © 2021,2022 IsardVDI S.L.
#   Copyright © 2022 Evilham <contact@evilham.com>
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
import os

from flask import flash, redirect, request, url_for
from flask_login import current_user, login_required, login_user, logout_user
from werkzeug.wrappers import Response

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from admin.flaskapp import AdminFlaskApp

from admin.views import render_template
from ..auth.authentication import *


def setup_login_views(app : "AdminFlaskApp") -> None:
    @app.route("/", methods=["GET", "POST"])
    @app.route("/login", methods=["GET", "POST"])
    def login() -> Response:
        if request.method == "POST":
            if request.form["user"] == "" or request.form["password"] == "":
                flash("Can't leave it blank", "danger")
            elif request.form["user"].startswith(" "):
                flash("Username not found or incorrect password.", "warning")
            else:
                ram_user = ram_users.get(request.form["user"])
                if ram_user and request.form["password"] == ram_user["password"]:
                    user = User(
                        id = ram_user["id"],
                        password = ram_user["password"],
                        role = ram_user["role"],
                        active = True,
                    )
                    login_user(user)
                    flash("Logged in successfully.", "success")
                    return redirect(url_for("web_users"))
                else:
                    flash("Username not found or incorrect password.", "warning")
        o : Response = app.make_response(render_template("login.html"))
        return o


    @app.route("/logout", methods=["GET"])
    @login_required
    def logout() -> Response:
        logout_user()
        return redirect(url_for("login"))
