#
#   Copyright © 2021,2022 IsardVDI S.L.
#   Copyright © 2022 Evilham <contact@evilham.com>
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import concurrent.futures
import json
import logging as log
import os
import sys
import time
import traceback
from pprint import pprint
from uuid import uuid4

import requests
from flask import (
    Response,
    jsonify,
    redirect,
    request,
    Response,
    send_file,
    url_for,
)
from flask_login import login_required

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from admin.flaskapp import AdminFlaskApp


from admin.views import render_template
from .decorators import is_admin

from ..lib.legal import gen_legal_if_not_exists


def setup_web_views(app : "AdminFlaskApp") -> None:
    @app.route("/users")
    @login_required
    def web_users() -> str:
        return render_template("pages/users.html", title="Users", nav="Users")


    @app.route("/roles")
    @login_required
    def web_roles() -> str:
        return render_template("pages/roles.html", title="Roles", nav="Roles")


    @app.route("/groups")
    @login_required
    def web_groups(provider : bool=False) -> str:
        return render_template("pages/groups.html", title="Groups", nav="Groups")


    @app.route("/avatar/<userid>", methods=["GET"])
    @login_required
    def avatar(userid : str) -> Response:
        if userid != "false":
            return send_file("../avatars/master-avatars/" + userid, mimetype="image/jpeg")
        return send_file("static/img/missing.jpg", mimetype="image/jpeg")


    @app.route("/dashboard")
    @login_required
    def dashboard(provider : bool=False) -> str:
        data = json.loads(requests.get("http://dd-sso-api/json").text)
        return render_template(
            "pages/dashboard.html", title="Customization", nav="Customization", data=data
        )


    @app.route("/legal")
    @login_required
    def legal() -> str:
        # data = json.loads(requests.get("http://dd-sso-api/json").text)
        return render_template("pages/legal.html", title="Legal", nav="Legal", data="")

    @app.route("/legal_text")
    def legal_text() -> str:
        lang = request.args.get("lang")
        if not lang or lang not in ["ca","es","en","fr"]:
            lang="ca"
        gen_legal_if_not_exists(app, lang)
        return render_template("pages/legal/"+lang)

    ### SYS ADMIN


    @app.route("/sysadmin/users")
    @login_required
    @is_admin
    def web_sysadmin_users() -> Response:
        o : Response = app.make_response(render_template(
            "pages/sysadmin/users.html", title="SysAdmin Users", nav="SysAdminUsers"
        ))
        return o


    @app.route("/sysadmin/groups")
    @login_required
    @is_admin
    def web_sysadmin_groups() -> Response:
        o : Response = app.make_response(render_template(
            "pages/sysadmin/groups.html", title="SysAdmin Groups", nav="SysAdminGroups"
        ))
        return o


    @app.route("/sysadmin/external")
    @login_required
    ## SysAdmin role
    def web_sysadmin_external() -> str:
        return render_template(
            "pages/sysadmin/external.html", title="External", nav="External"
        )


    @app.route("/sockettest")
    def web_sockettest() -> str:
        return render_template(
            "pages/sockettest.html", title="Sockettest Users", nav="SysAdminUsers"
        )
