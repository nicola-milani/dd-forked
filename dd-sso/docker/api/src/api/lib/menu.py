#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import json
import logging
import pprint
import time
import traceback
from datetime import datetime, timedelta

import yaml
from api import app as application
from jinja2 import Environment, FileSystemLoader


def write_css():
    env = Environment(loader=FileSystemLoader("api/static/_templates"))
    css_template = env.get_template("dd.css")
    with open("menu/custom.yaml", "r") as menu_custom_file:
        menu_custom_yaml = menu_custom_file.read()
    menu_custom = yaml.full_load(menu_custom_yaml)
    css = css_template.render(data=menu_custom)
    with open("api/static/css/dd.css", "w") as css_file:
        css_file.write(css)


class Menu:
    def __init__(self):
        # self.user_menudict=self.gen_user_menu()
        # pprint.pprint(self.user_menudict)
        # self.write_user_menu()

        self.menudict = self.gen_header()
        pprint.pprint(self.menudict)
        self.write_headers()
        write_css()

    """ HEADER & APP MENU """

    def gen_header(self):
        with open(r"menu/system.yaml") as yml:
            system = yaml.load(yml, Loader=yaml.FullLoader)

        user_menu = []
        for item in system["user_menu"]:
            item["href"] = (
                "https://"
                + item["subdomain"]
                + "."
                + application.config["DOMAIN"]
                + item["href"]
            )
            del item["subdomain"]
            user_menu.append(item)
        user_menu_dict = {
            "user_menu": user_menu,
            "user_avatar": "https://sso."
            + application.config["DOMAIN"]
            + "/auth/realms/master/avatar-provider",
        }

        apps_internal = []
        for app in system["apps_internal"]:
            app_href = app["href"] if app.get("href", "") else "/"
            # Only modify href if it is a relative URL
            if app_href.startswith("/") or app_href.startswith("#"):
                # Get the subdomain part, not using it if it is missing
                subdomain_part = ""
                if "subdomain" in app:
                    # We pop 'subdomain' so it is not in apps_internal
                    # note this includes a trailing dot if defined
                    subdomain_part = f"{ app.pop('subdomain') }.".lstrip(".")
                # Actually construct the full URL
                app["href"] = "".join([
                    "https://",
                    subdomain_part,
                    application.config["DOMAIN"],
                    app_href,
                ])
            apps_internal.append(app)

        with open(r"menu/custom.yaml") as yml:
            custom = yaml.load(yml, Loader=yaml.FullLoader)
        custom["background_login"] = (
            "https://api." + application.config["DOMAIN"] + custom["background_login"]
        )
        custom["logo"] = "https://api." + application.config["DOMAIN"] + custom["logo"]
        custom["product_logo"] = (
            "https://api." + application.config["DOMAIN"] + custom["product_logo"]
        )

        menudict = {**custom, **{"apps_internal": apps_internal, **user_menu_dict}}
        menudict["user"] = {}
        menudict["user"]["account"] = (
            "https://sso." + application.config["DOMAIN"] + system["user"]["account"]
        )
        menudict["user"]["avatar"] = (
            "https://sso." + application.config["DOMAIN"] + system["user"]["avatar"]
        )
        return menudict

    def write_headers(self):
        env = Environment(loader=FileSystemLoader("api/static/_templates"))

        template = env.get_template("user_menu.html")
        output_from_parsed_template = template.render(data=self.menudict)
        print(output_from_parsed_template)
        with open("api/static/templates/user_menu_header.html", "w") as fh:
            fh.write(output_from_parsed_template)

        # with open("api/static/templates/user_menu_header.json", "w") as fh:
        #     fh.write(json.dumps(self.menudict))

        template = env.get_template("apps_menu.html")
        output_from_parsed_template = template.render(data=self.menudict)
        print(output_from_parsed_template)
        with open("api/static/templates/header.html", "w") as fh:
            fh.write(output_from_parsed_template)

        ## Nextcloud. Nginx will serve /header/html/nextcloud -> header_nextcloud.html
        with open("api/static/templates/header_nextcloud.html", "w") as fh:
            fh.write(output_from_parsed_template)
        with open("api/static/templates/header_nextcloud.html", "a") as fh:
            with open("api/static/_templates/nextcloud.html", "r") as nextcloud:
                fh.write(nextcloud.read())
        with open("api/static/templates/header.json", "w") as fh:
            fh.write(json.dumps(self.menudict))

        ## Admin app. Nginx will serve /header/html/admin -> header_admin.html
        template = env.get_template("admin.html")
        output_from_parsed_template = template.render(data=self.menudict)
        print(output_from_parsed_template)
        with open("api/static/templates/header_admin.html", "w") as fh:
            fh.write(output_from_parsed_template)

        ## SSO app. Nginx will serve /header/html/sso -> header_sso.html
        template = env.get_template("sso.html")
        output_from_parsed_template = template.render(data=self.menudict)
        print(output_from_parsed_template)
        with open("api/static/templates/header_sso.html", "w") as fh:
            fh.write(output_from_parsed_template)

    def get_header(self):
        return self.menudict
        # with open('menu.yaml', 'w') as yml:
        #     print(yaml.dump(header, yml, allow_unicode=True))
