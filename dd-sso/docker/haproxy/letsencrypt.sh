#!/bin/sh
#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
if [ ! -L /etc/letsencrypt/renewal-hooks/deploy/letsencrypt-hook-deploy-concatenante.sh ]
then
  mkdir -p /etc/letsencrypt/renewal-hooks/deploy/
  ln -s /usr/local/sbin/letsencrypt-hook-deploy-concatenante.sh /etc/letsencrypt/renewal-hooks/deploy/
fi

if [ -n "$LETSENCRYPT_DOMAIN" -a -n "$LETSENCRYPT_EMAIL" ]
then
  LETSENCRYPT_DOMAIN="$LETSENCRYPT_DOMAIN" crond
  if [ "$LETSENCRYPT_DOMAIN_ROOT" == "true" ]
  then
    option_root_domain="-d $LETSENCRYPT_DOMAIN"
  fi
  if [ ! -f /certs/chain.pem ]
  then
    if certbot certonly --standalone -m "$LETSENCRYPT_EMAIL" -n --agree-tos \
      -d "sso.$LETSENCRYPT_DOMAIN" \
      -d "api.$LETSENCRYPT_DOMAIN" \
      -d "admin.$LETSENCRYPT_DOMAIN" \
      -d "moodle.$LETSENCRYPT_DOMAIN" \
      -d "nextcloud.$LETSENCRYPT_DOMAIN" \
      -d "wp.$LETSENCRYPT_DOMAIN" \
      -d "oof.$LETSENCRYPT_DOMAIN" \
      -d "pad.$LETSENCRYPT_DOMAIN" \
      $option_root_domain
    then
      RENEWED_LINEAGE="/etc/letsencrypt/live/sso.$LETSENCRYPT_DOMAIN" /etc/letsencrypt/renewal-hooks/deploy/letsencrypt-hook-deploy-concatenante.sh
    fi
  fi
fi
