# DD theme for keycloak

When this started, it was heavily inspired by the liiibrelite theme at:
https://forge.liiib.re/indiehost/tech/keycloak-themes/

But due to licensing and the fact that it didn't match our needs, we switched
to just heavily customising and overriding keycloak's base theme:
https://github.com/keycloak/keycloak/tree/main/themes/src/main/resources/theme/base
