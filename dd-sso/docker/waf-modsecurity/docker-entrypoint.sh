#!/bin/bash -e
set -e

if [ "${DISABLE_WAF:-true}" == "true" ]; then
	sed -i.orig -e "s/modsecurity On/modsecurity Off/" /etc/apache2/sites-available/000-default.conf
else
	sed -i.orig -e "s/modsecurity Off/modsecurity On/" /etc/apache2/sites-available/000-default.conf
fi

apachectl -D FOREGROUND
