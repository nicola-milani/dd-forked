# Personalitzacions

## Enllaços al Megamenú

[El Megamenú][megamenu-internes] te dues parts:

- [Interna / de sistema][megamenu-internes]
- [Externa / configurable][megamenu-externes]

[megamenu-internes]: https://dd.digitalitzacio-democratica.xnet-x.net/manual-usuari/menu-principal-dd/
[megamenu-externes]: https://dd.digitalitzacio-democratica.xnet-x.net/manual-usuari/eines-externes-megamenu/

Els enllaços [externs del Megamenú][megamenu-externes] es poden modificar fent
servir la interfície a `https://admin.DOMINI`, els enllaços
[interns][megamenu-internes] no.

En ambdós casos es pot fer aprovisionament o personalització avançada d'aquests
enllaços fent servir els fitxers `custom/menu/custom.yaml` i
`custom/menu/system.yaml`.

Un exemple d'aquests fitxers, i el seu contingut per defecte es troba a
`custom.sample/menu/custom.yaml` i `custom.sample/menu/system.yaml`.

### `custom/menu/system.yaml`

Pel fitxer `custom/menu/system.yaml` cal tenir en compte que en funció dels
valors de `href` i i la presència o absència de `subdomain`, els enllaços es
generen de manera diferent.

També cal tenir en compte que les adreces relatives han de començar amb
`/` o `#`.

```yaml
# Exemple de apps_internal a custom/menu/system.yaml
apps_internal:
# Això resulta en un enllaç https://moodle.DOMINI/ADREÇA
- subdomain: moodle
  href: /ADREÇA
  icon: fa fa-graduation-cap
  name: Aules
  shortname: courses
# Això resulta en un enllaç https://ADREÇA_EXTERNA
# perquè href és un enllaç absolut
- href: https://ADREÇA_EXTERNA
  icon: fa fa-book
  name: Manual
  shortname: manual
# Això resulta en un enllaç https://DOMINI/ADREÇA_DOMINI_PRINCIPAL
- href: /ADREÇA_DOMINI_PRINCIPAL
  icon: fa fa-rss
  name: Quant a
  shortname: about
```


## Peu de pàgina en el login

El peu de pàgina fa part del tema de login de Keycloak, per tal de modificar-lo,
cal fer servir un tema amb base 'dd'.

Per facilitar-lo, al directori `dd-sso/docker/keycloak/themes` trobem:

- `dd-custom`: un directori buit, el que posem aquí Keycloak ho interpretarà
  com un nou tema `dd-custom`.
- `dd-custom.sample`: un exemple de com es prepararia el tema `dd-custom`, per
  tal de reemplaçar el footer.

Així doncs, per personalitzar el footer, copiarem el contingut de
`dd-custom.sample` a `dd-custom`, i editem `dd-custom/login/dd-footer.ftl`
d'acord amb les nostres necessitats.

Un cop fet això, a la interfície d'administració de Keycloak haurem de triar
`dd-custom` com a tema d'inici de sessió a:

`https://sso.DOMINI/auth/admin/master/console/#/realms/master/theme-settings`

> **Nota:** el directori dd-custom no s'actualitzarà mai, és responsabilitat
> vostra revisar els canvis al tema `dd` i al directori  `dd-custom.sample`
> per tal de mantenir la compatibilitat amb els vostres canvis.
