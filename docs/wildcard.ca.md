# Instal·lació d'un certificat propi wildcard

Abans de res, atura la suite mitjançant la comanda del dd-ctl down:

`/opt/src/DD# ./dd-ctl down`

Per que el certificat sigui compatible amb DD, heu de fusionar el fullchain amb la key privada del certificat, la opció preferida es simplement imprimir els dos fitxers en un:

`/tmp/certificat# cat fullchain.pem cert.key > /opt/DD/src/haproxy/certs/chain.pem`

El fitxer fullchain.pem ha de contenir tota la cadena del certificat, la cert.key es la clau privada, ha de quedar mes o menys així:

```
> cat /opt/DD/src/haproxy/certs/chain.pem
-----BEGIN CERTIFICATE-----
YDC ...
...
... PnQP
-----END CERTIFICATE-----

-----BEGIN CERTIFICATE-----
5dSf ...
...
... Hwgs
-----END CERTIFICATE-----

-----BEGIN CERTIFICATE-----
sI3q ...
...
... vZas
-----END CERTIFICATE-----

-----BEGIN RSA PRIVATE KEY-----
vzKJ ...
...
... 2dLs
-----END RSA PRIVATE KEY-----
```

Reviseu bé la ruta on heu copiat el fitxer chain.pem, que ha de ser /opt/DD/src/haproxy/certs

Un cop fet això arrenquem de nou la suite amb la comanda dd-ctl up:

`/opt/src/DD# ./dd-ctl up`

I ja podem disfrutar del nostre DD correctamente instal·lat amb un certificat propi.