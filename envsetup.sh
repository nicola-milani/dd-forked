export TITLE="itservicenet"
export TITLE_SHORT="itservicenet"
export DD_DOMAIN=dd.nicolamilani.it
export DOMAIN=dd.nicolamilani.it
export LANGUAGE_CODE=en
export LETSENCRYPT_DOMAIN_ROOT=true
export LETSENCRYPT_DNS=dd.nicolamilani.it
export LETSENCRYPT_EMAIL=info@nicolamilani.it